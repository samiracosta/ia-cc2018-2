#ifndef DEFUZZIFIER_H
#define DEFUZZIFIER_H

#include <QVector>
#include <QString>
#include <QtDebug>


class Defuzzifier
{
public:
    Defuzzifier();

    QString GetRiskDefuzzification(QVector<double> rulesResult);
};

#endif // DEFUZZIFIER_H
