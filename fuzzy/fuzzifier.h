#ifndef FUZZIFIER_H
#define FUZZIFIER_H
#include <QVector>
#include <QtDebug>

class Fuzzifier
{
public:
    Fuzzifier();

    QVector<double> GetMoneyFuzzyfication(int money);
    QVector<double> GetPeopleFuzzyfication(int people);

};

#endif // FUZZIFIER_H
