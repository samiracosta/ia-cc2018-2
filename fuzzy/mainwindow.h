#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <fuzzifier.h>
#include <ruler.h>
#include <defuzzifier.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnCalc_clicked();

private:
    Ui::MainWindow *ui;

    Fuzzifier *m_fuzzifier;
    Ruler *m_ruler;
    Defuzzifier *m_defuzzifier;
};

#endif // MAINWINDOW_H
