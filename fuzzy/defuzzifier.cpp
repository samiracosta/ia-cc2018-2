#include "defuzzifier.h"
#include <QtDebug>

Defuzzifier::Defuzzifier()
{

}


QString Defuzzifier::GetRiskDefuzzification(QVector<double> rulesResult){
    QString risk;
    double cog; //center of Gravity
    double numerator = 0;
    double denominator = 0;

    QVector<int> impulses;
    impulses.append(10);
    impulses.append(20);
    impulses.append(50);
    impulses.append(60);
    impulses.append(70);
    impulses.append(80);
    impulses.append(90);

    int k = 0;
    qDebug()<<"linha 25";
    for(int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            numerator += impulses.at(j+k)*rulesResult.at(i);
            denominator += rulesResult.at(i);
        }

        k += 3;
    }
qDebug()<<"linha 34";
    cog = numerator/denominator;

    double low;
    double medium;
    double high;

    if (cog <= 30){
        low = 1.0;
        medium = 0.0;
        high = 0.0;
    }else if((cog > 30) & (cog <= 40)){
        low = (40.0 - cog)/(40.0 - 30.0);
        medium = 1.0;
        high = 0.0;
    }else if ((cog >= 40) & (cog <= 60)){
        low = 0.0;
        medium = 1.0;
        high = 0.0;

    }else if ((cog >= 60) & (cog <= 70)){
        low = 0.0;
        medium = (70 - cog)/(70.0 - 60.0);
        high = (cog - 60.0)/(70.0 - 60.0);
    }else{
        low = 0.0;
        medium = 0.0;
        high = 1.0;
    }


    if ((low >= medium) & (low >= high)){
        risk = "BAIXO";
    }else if (medium >= high){
        risk = "MÉDIO";
    }else{
        risk = "ALTO";
    }

    return risk;
}
