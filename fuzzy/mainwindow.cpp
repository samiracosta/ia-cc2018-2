#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_fuzzifier = new Fuzzifier();

   //  qDebug() <<m_fuzzifier->GetMoneyFuzzyfication(35);
   //  qDebug() <<m_fuzzifier->GetPeopleFuzzyfication(60);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnCalc_clicked()
{
//Fuzzification
// Grau de pertinencia = Degree of Membership

QVector<double> moneyMembership = m_fuzzifier->GetMoneyFuzzyfication(ui->spnMoney->value());
QVector<double> peopleMembership = m_fuzzifier->GetPeopleFuzzyfication(ui->spnPeople->value());

//Rules
//Aplicando regras
    QVector<double> rulesResult = m_ruler->GetRulesResult(moneyMembership,peopleMembership);


//Deffuzification
    //Aplicando a defuzificação

ui->lblrisk->setText(m_defuzzifier->GetRiskDefuzzification(rulesResult));


}
