#include "hebb.h"

Hebb::Hebb()
{

}

QVector<double> Hebb::CalculateWeights(QMap<QVector<int>, int> trainingSet){

    QVector<double> weights;

    //Number of inputs
    //Número de entradas
    int numOfInputs = trainingSet.begin().key().size();

    //Initializing weight vector //Inicializando o vetor de pessos
    for (int i = 0; i < numOfInputs; ++i) {

        weights.append(0);
    }

    //Calculate weights
    QMap<QVector<int>, int>::iterator it;
    for (it = trainingSet.begin(); it != trainingSet.end(); it++) {

        // The map is a table. This table presents the inputs as the key
        // and the target as the value
        QVector<int> inputs = it.key();
        int target = it.value();

        for (int i = 0; i < numOfInputs; ++i) {

            //Canvas item 8.3.1
            weights[i] = weights.at(i) + inputs.at(i)*target;
        }

    }

    return weights;
}
