#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  ui->repetitionSpinBox->setEnabled(false);
  ui->resultsMultiRunGroupBox->setEnabled(false);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_multiRunRadioButton_toggled(bool checked)
{
  if (checked)
  {
    ui->repetitionSpinBox->setEnabled(true);
    ui->repetitionSpinBox->setValue(100);
    ui->resultsSingleRunGroupBox->setEnabled(false);
    ui->resultsMultiRunGroupBox->setEnabled(true);
  }
  else
  {
    ui->repetitionSpinBox->setEnabled(false);
    ui->repetitionSpinBox->setValue(1);
    ui->resultsSingleRunGroupBox->setEnabled(true);
    ui->resultsMultiRunGroupBox->setEnabled(false);
  }
}

void MainWindow::on_runPushButton_clicked()
{  

  double minFunction = 421.0;
  double gaAnswer = 0.0;
  int correct = 0;
  int incorrect = 0;

  Ga *ga = new Ga(ui->populationSpinBox->value(),
                  ui->crossoverDoubleSpinBox->value(),
                  ui->mutationDoubleSpinBox->value(),
                  ui->generationSpinBox->value(),
                  ui->elitismCheckBox->isChecked());

  QProgressDialog progressDlg("Repetitions...",
                              "Cancel",
                              0,
                              ui->repetitionSpinBox->value(),
                              this);
  progressDlg.setWindowModality(Qt::WindowModal);

  for (int i = 0; i < ui->repetitionSpinBox->value(); i++)
  {
    gaAnswer = ga->RunGenerations();

    if(abs(gaAnswer - minFunction) < std::numeric_limits<double>::epsilon())
      correct += 1;
    else
      incorrect += 1;

    progressDlg.setValue(i);

    if (progressDlg.wasCanceled())
    {
      break;
    }
  }

  progressDlg.setValue(ui->repetitionSpinBox->value());

  if(ui->singleRunRadioButton->isChecked())
  {
    ui->xResultLabel->setText(QString::number(gaAnswer));
    ui->fxLabel->setText(QString::number(abs(gaAnswer*sin(sqrt(abs(gaAnswer))))*(-1)));
    if(abs(gaAnswer - minFunction) < std::numeric_limits<double>::epsilon())
      ui->iconResultLabel->setPixmap(QPixmap(":/icons/correct-icon.png"));
    else
      ui->iconResultLabel->setPixmap(QPixmap(":/icons/incorrect-icon.png"));
  }
  else
  {
    ui->correctResultsLabel->setText(QString::number(correct));
    ui->incorrectResultsLabel->setText(QString::number(incorrect));

  }

}
