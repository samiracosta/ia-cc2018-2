#include "ga.h"

Ga::Ga(int popSize, double crossoverRate, double mutationRate, int generations, bool elitism)
{
  m_popSize = popSize;
  m_crossoverRate = crossoverRate;
  m_mutationRate = mutationRate;
  m_generations = generations;
  m_elitism = elitism;

  m_population = QVector<double>();
  m_stringPopulation = QVector<QString>();
  m_fitness = QVector<double>();
  m_selected = QVector<double>();
  m_allX = QVector<double>();
  m_sumFitness = 0.0;
  m_bestIndividual = 0.0;


  // 10 - number of bits
  // 2^10 = 1024 possibilities of x
  // 512 - upper bound
  m_granularity = pow(2, 10);
  m_step = 512.0/m_granularity;

  // Getting all x
  GetAllX();

}

void Ga::GetAllX (void)
{
  m_allX = QVector<double>();

  for (int i = 0; i < m_granularity; i++)
  {
    m_allX.append(i*m_step);
  }

}


QString Ga::GetBin(int decimal)
{
  QString binary;
  binary.setNum(decimal, 2);
  int s = binary.size();

  // 10 - number of bits
  for(int i = 0; i < (10 - s); i++)
    binary.prepend('0');

  return binary;
}

double Ga::GetDecimal (QString binary)
{
  bool ok;
  int decimal = binary.toInt(&ok, 2);

  return decimal*m_step;

}

void Ga::GetInitialPopulation(void)
{
  QVector<int> sampled = QVector<int>();
  m_population = QVector<double>();

  for (int i = 0; i < m_popSize; i++)
  {
    int index = QRandomGenerator::global()->bounded(static_cast<int>(m_granularity));

    while (sampled.contains(index))
    {
      index = QRandomGenerator::global()->bounded(static_cast<int>(m_granularity));
    }

    m_population.append(m_allX.at(index));
    sampled.append(index);
  }

}

void Ga::Evaluate (void)
{
  m_fitness = QVector<double>();
  m_sumFitness = 0.0;

  for (int i = 0; i < m_popSize; i++)
  {
    // Inserir como a função de aptidão é calculada
    // ...
  }

  // Seleciona o melhor indivíduo para ser utilizado,
  // caso o elistimo esteja selecionado
  m_bestIndividual = m_population.at(m_fitness.indexOf(*std::min_element(m_fitness.constBegin(), m_fitness.constEnd())));
}

void Ga::Select (void)
{
  //Seleciona os indivíduos para a recombinação

}


void Ga::Cross(void)
{
  // Aplica a operação de cruzamento
}

void Ga::Mutate (void)
{
  // Aplica a operação de mutação
}


double Ga::RunGenerations(void)
{
  // Método de controle do Algoritmo Genético (ver fluxograma no Canvas).

  return  m_bestIndividual;
}

