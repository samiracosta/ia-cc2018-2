#ifndef GA_H
#define GA_H

#include <QVector>
#include <QDebug>
#include <QRandomGenerator>

#include <math.h>
#include <algorithm>


class Ga
{
public:
  Ga(int popSize, double crossoverRate, double mutationRate, int generations, bool elitism);
  double RunGenerations(void);


private:
  void GetAllX(void);
  QString GetBin(int decimal);
  double GetDecimal (QString binary);
  void GetInitialPopulation(void);
  void Evaluate(void);
  void Select(void);
  void Cross(void);
  void Mutate (void);

  int    m_popSize;
  int    m_generations;
  double m_sumFitness;
  double m_crossoverRate;
  double m_mutationRate;
  double m_granularity;
  double m_step;
  double m_bestIndividual;
  bool m_elitism;

  QVector<double> m_population;
  QVector<QString> m_stringPopulation;
  QVector<double> m_fitness;
  QVector<double> m_selected;
  QVector<double> m_allX;

};

#endif // GA_H
